<?php

$pages = get_pages();
$pagesArray = array();
foreach($pages as $page){
  $pagesArray[get_permalink($page->ID)] = $page->post_title;
}

/*
Title: Landing Page Fields
Post Type: landing-page
*/
piklist('field', array(
  'type' => 'text',
  'field' => 'primary_moto',
  'label' => 'Primary Text',
  'description' => 'The primary text of your landing page/section.',
  'attributes' => array(
    'class' => 'regular-text' // WordPress css class
  )
));

piklist('field', array(
  'type' => 'text',
  'field' => 'secondary_moto',
  'add_more' => true,
  'label' => 'Secondary Text',
  'description' => 'The secondary text of your landing page/section.',
  'attributes' => array(
    'class' => 'regular-text' // WordPress css class
  )
));

piklist('field', array(
  'type' => 'file',
  'field' => 'background_image',
  'label' => 'Add Background',
  'description' => 'The background image of your landing page/section.',
  'options' => array(
    'basic' => false // set field to basic uploader
  )
));

piklist('field', array(
  'type' => 'group',
  'field' => 'action_buttons',
  'add_more' => true,
  'label' => 'Add Buttons',
  'description' => 'The buttons at the bottom of your landing page/section.',
  'fields' => array(
    array(
      'type' => 'text',
      'field' => 'button_text',
      'label' => 'Text of the button.',
    ),
    array(
      'type' => 'select',
      'field' => 'button_href',
      'label' => 'Link location of the button.',
      'choices' => $pagesArray
    ),
    array(
      'type' => 'select',
      'field' => 'button_color',
      'label' => 'Colour of the button',
      'choices' => array(
        'primary' => 'Secondary Color',
        'secondary' => 'Success Color',
        'success' => 'Alert Color',
        'disabled' => 'Disabled Button',
      )
    ),
    array(
      'type' => 'select',
      'field' => 'button_shape',
      'label' => 'Shape of the button',
      'default' => 'normal',
      'choices' => array(
        'round' => 'Round Button',
        'radius' => 'Curved button',
        'normal' => 'Rectangle Button',
      )
    ),
    array(
      'type' => 'checkbox',
      'field' => 'button_hollow',
      'choices' => array(
        'first' => 'Hollow'
      )
    ),
  )
));
