<?php
$postTypeArgsArray = array(
  array(
    'post_type' => 'landing-page',
    'name' => 'homepage'
  )
);

foreach ($postTypeArgsArray as $postTypeArgs):
  $query = new WP_Query($postTypeArgs);
  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
  <div class="section-<?php echo get_the_id() ?>">
    <?php get_template_part('templates/partials/frontpage-' . $postTypeArgs['post_type']); ?>
  </div>
<?php endwhile; wp_reset_postdata(); endif; ?>
<?php endforeach; ?>
