<?php
$postMeta = get_post_meta(get_the_id());
?>

<section class="landing-section" data-interchange="[<?php echo wp_get_attachment_image_src($postMeta['background_image'][0], [1920, 1080])[0] ?>, small]">
  <div class="landing-content">
    <h1 class="primary-moto"><?php echo $postMeta['primary_moto'][0]; ?></h1>
    <h3 class="secondary-moto"><?php echo $postMeta['secondary_moto'][0]; ?></h3>
    <div class="landing-buttons">
      <a class="round hollow button">Learn More</a>
      <a class="round button">Git(intended) in touch</a>
    </div>
  </div>
</section>
